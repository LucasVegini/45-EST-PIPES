package br.udesc.ceavi.ttt.pipes;

import java.util.stream.Stream;
import javax.swing.table.AbstractTableModel;

/**
 *
 */
public class Tabuleiro extends AbstractTableModel {
    
    private final Celula[][] tabuleiro;
    
    private int largura;
    private int altura;

    public Tabuleiro(int largura, int altura) {
        this.largura = largura;
        this.altura = altura;
        this.tabuleiro = new Celula[largura][];
        for (int i = 0; i < largura; i++) {
            this.tabuleiro[i] = new Celula[altura];
        }
    }
    
    public void setCelula(int x, int y, Celula celula){
        this.tabuleiro[x][y] = celula;
    }

    public Celula getCelula(int x, int y) {
        return this.tabuleiro[x][y];
    }

    @Override
    public String toString() {
        return Stream.of(this.tabuleiro).reduce("", (String t, Celula[] u) -> {
            return t + Stream.of(u).reduce("", (String t1, Celula u1) -> {
                return t1 + u1.toString() + ",";
            }, (String t1, String u1) -> {
                return t1 + u1;
            }) + "\n";
        }, (String t, String u) -> {
            return t + u;
        });
    }

    @Override
    public int getRowCount() {
        return this.altura;
    }

    @Override
    public int getColumnCount() {
        return this.largura;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return this.tabuleiro[columnIndex][rowIndex].getSprite();
    }
    
}
