package br.udesc.ceavi.ttt.pipes;

/**
 *
 */
public class CelulaDesconhecidaException extends Exception {

    public CelulaDesconhecidaException(char tipo) {
        super("Célula de tipo " + tipo + " desconhecida.");
    }
    
}
