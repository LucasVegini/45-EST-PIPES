package br.udesc.ceavi.ttt.pipes;

import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * Classe base da aplicação
 */
public class PIPES {
    
    public static final String NOME_APLICACAO = "PIPES";
    public static final boolean DEBUG = true;
    public static final int LARGURA = 3;
    public static final int ALTURA  = 3;
    
    private JanelaAplicacao janelaPrincipal;
    private Tabuleiro tabuleiro;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {}
        (new PIPES()).inicia();
    }

    private void inicia() {
        this.janelaPrincipal = new JanelaAplicacao(NOME_APLICACAO, this);
        this.janelaPrincipal.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.janelaPrincipal.setVisible(true);
    }

    public void carregaArquivo(File file) {
        (new Thread(new LeitorArquivoPIPES(file, this))).start();
    }
    
    public void carregaTabuleiro(Tabuleiro tabuleiro){
        this.tabuleiro = tabuleiro;
        this.janelaPrincipal.carregaTabuleiro(tabuleiro);
        if(PIPES.DEBUG){
            System.out.println(tabuleiro);
        }
        this.notificaInfo("Tabuleiro carregado com sucesso!");
    }
    
    public void notificaInfo(String mensagem){
        JOptionPane.showMessageDialog(null, mensagem, "Sucesso", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public void notificaErro(String mensagem, Exception ex){
        if(PIPES.DEBUG){
            ex.printStackTrace(System.err);
        }
        notificaErro(mensagem);
    }
    
    public void notificaErro(String mensagem){
        JOptionPane.showMessageDialog(null, mensagem, "Erro", JOptionPane.ERROR_MESSAGE);
    }
    
    public String getTextoSobre(){
        return "Desenvolvido por:\nAfonso Ueslei Boing e Lucas Ramthum Vegini\nEmail";
    }

    void rotacionaCano(int selectedColumn, int selectedRow) {
        if(selectedColumn < 0 || selectedRow < 0){
            return;
        }
        Celula celula = this.tabuleiro.getCelula(selectedColumn, selectedRow);
        int orientacao = celula.getOrientacao().getOrientacaoNum() + 1;
        if(orientacao > 4){
            orientacao = 1;
        }
        celula.setOrientacao(Orientacao.fromNum(orientacao));
        this.janelaPrincipal.repintaTabuleiro();
    }

    void buscaLargura() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    void buscaProfundidade() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
