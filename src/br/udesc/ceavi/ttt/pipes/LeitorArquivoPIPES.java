package br.udesc.ceavi.ttt.pipes;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 *
 */
public class LeitorArquivoPIPES implements Runnable {
    
    private final File file;
    private final PIPES jogo;
    
    public LeitorArquivoPIPES(File file, PIPES jogo){
        this.file = file;
        this.jogo = jogo;
    }

    @Override
    public void run() {
        try {
            List<String> linhas = Files.readAllLines(Paths.get(this.file.getPath()));
            if(linhas.size() != PIPES.ALTURA){
                throw new ConteudoInvalidoException(linhas);
            }
            Tabuleiro tabuleiro = new Tabuleiro(PIPES.LARGURA, PIPES.ALTURA);
            for (int y = 0; y < linhas.size(); y++) {
                String linha = linhas.get(y);
                if(!linha.matches("([XTLI]\\d,){" + PIPES.LARGURA + "}")){
                    throw new ConteudoInvalidoException(linhas);
                }
                String[] colunas = linha.split(",");
                for (int x = 0; x < colunas.length; x++) {
                    String coluna = colunas[x];
                    tabuleiro.setCelula(x, y, Celula.fromString(coluna));
                }
            }
            this.jogo.carregaTabuleiro(tabuleiro);
        } catch (IOException|ConteudoInvalidoException|CelulaDesconhecidaException ex) {
            this.jogo.notificaErro("Não foi possível ler o arquivo.", ex);
        }
    }
    
}
