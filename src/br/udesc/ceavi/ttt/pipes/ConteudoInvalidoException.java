package br.udesc.ceavi.ttt.pipes;

import java.util.List;

/**
 *
 */
public class ConteudoInvalidoException extends Exception {
    
    private List<String> conteudo;
    
    public ConteudoInvalidoException(List<String> conteudo){
        super("Conteúdo do arquivo inválido...");
        this.conteudo = conteudo;
    }

    @Override
    public String getMessage() {
        if(PIPES.DEBUG){
            return super.getMessage() + this.getConteudoString();
        }
        return super.getMessage();
    }

    private String getConteudoString() {
        return this.conteudo.stream().reduce("", (t, u) -> {
            return t + "\n" + u;
        });
    }
    
}
